/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Shell/Executor/Entity/Std/Cleanup.h>

#include <system_error>

#include <GitComplex/Domain/Std/Export.h>

/*!
 * \file include/GitComplex/Shell/Executor/Entity/Std/Detail/Strategy/PeriodicRepeat.h
 * \brief include/GitComplex/Shell/Executor/Entity/Std/Detail/Strategy/PeriodicRepeat.h
 */

namespace GitComplex::Shell::Executor::Entity::Std::Detail::Strategy
{

/*!
 * \class PeriodicRepeat
 * \brief PeriodicRepeat.
 *
 * \headerfile GitComplex/Shell/Executor/Entity/Std/Detail/Strategy/PeriodicRepeat.h
 *
 * PeriodicRepeat.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Std]]
 */
class GITCOMPLEX_DOMAIN_STD_EXPORT
        PeriodicRepeat
{
public:
    explicit PeriodicRepeat(std::size_t attempt_count,
                            std::chrono::milliseconds wait_timeout)
        : m_attempt{attempt_count},
          m_wait_timeout{wait_timeout}
    {}

    std::chrono::milliseconds waitTimeout() const noexcept;
    bool    repeatOn(const std::error_code& error) const noexcept;
    Cleanup cleanup() const noexcept;

private:
    mutable std::size_t       m_attempt;
    std::chrono::milliseconds m_wait_timeout;
};

} // namespace GitComplex::Shell::Executor::Entity::Std::Detail::Strategy
