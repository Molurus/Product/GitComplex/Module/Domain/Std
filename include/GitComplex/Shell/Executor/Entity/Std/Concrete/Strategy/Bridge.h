/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Shell/Executor/Entity/Std/Strategy.h>

/*!
 * \file include/GitComplex/Shell/Executor/Entity/Std/Concrete/Strategy/Bridge.h
 * \brief include/GitComplex/Shell/Executor/Entity/Std/Concrete/Strategy/Bridge.h
 */

namespace GitComplex::Shell::Executor::Entity::Std::Concrete::Strategy
{

/*!
 * \class Bridge
 * \brief Bridge.
 *
 * \headerfile GitComplex/Shell/Executor/Entity/Std/Concrete/Strategy/Bridge.h
 *
 * Bridge.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Entity]] [[Framework::Std]]
 */
template <class Detail>
class Bridge final : public Entity::Std::Strategy
{
public:
    template <class ... Args>
    explicit Bridge(Args ... args)
        : m_detail{std::forward<Args>(args) ...}
    {}

private:
    std::chrono::milliseconds waitTimeout() const noexcept        override
    { return m_detail.waitTimeout(); }
    bool    repeatOn(const std::error_code& error) const noexcept override
    { return m_detail.repeatOn(error); }
    Cleanup cleanup() const noexcept override
    { return m_detail.cleanup(); }

    Detail m_detail;
};

} // namespace GitComplex::Shell::Executor::Entity::Std::Concrete::Strategy
