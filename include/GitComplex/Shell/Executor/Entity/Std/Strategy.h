/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Shell/Executor/Entity/Std/Cleanup.h>

#include <system_error>

/*!
 * \file include/GitComplex/Shell/Executor/Entity/Std/Strategy.h
 * \brief include/GitComplex/Shell/Executor/Entity/Std/Strategy.h
 */

namespace GitComplex::Shell::Executor::Entity::Std
{

/*!
 * \class Strategy
 * \brief Strategy.
 *
 * \headerfile GitComplex/Shell/Executor/Entity/Std/Strategy.h
 *
 * Strategy.
 *
 * \note [[Abstraction::Interface]] [[Viper::Entity]] [[Framework::Std]]
 */
class Strategy
{
public:
    virtual ~Strategy() = default;

    virtual std::chrono::milliseconds waitTimeout() const noexcept        = 0;
    virtual bool    repeatOn(const std::error_code& error) const noexcept = 0;
    virtual Cleanup cleanup() const noexcept = 0;
};

} // namespace GitComplex::Shell::Executor::Entity::Std
