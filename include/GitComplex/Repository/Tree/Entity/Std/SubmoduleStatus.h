/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Core/Std/Path.h>
#include <GitComplex/Repository/Entity/Std/Branch.h>
#include <GitComplex/Repository/Entity/Std/Sha.h>
#include <GitComplex/Repository/Tree/Entity/Std/Namespace.h>

/*!
 * \file include/GitComplex/Repository/Tree/Entity/Std/SubmoduleStatus.h
 * \brief include/GitComplex/Repository/Tree/Entity/Std/SubmoduleStatus.h
 */

namespace GitComplex::Repository::Tree::Entity::Std
{

/*!
 * \struct SubmoduleStatus
 * \brief SubmoduleStatus.
 *
 * \headerfile GitComplex/Repository/Tree/Entity/Std/SubmoduleStatus.h
 *
 * SubmoduleStatus.
 *
 * \note [[Abstraction::DataType]] [[Viper::Entity]] [[Framework::Std]]
 */
struct SubmoduleStatus
{
    Path   path;
    Sha    sha;
    Branch branch;
    char   prefix;
    bool   hasModifications;
};

/*!
 * SubmoduleStatusList.
 *
 * \note [[Abstraction::DataType]] [[Viper::Entity]] [[Framework::Std]]
 */
using SubmoduleStatusList = std::vector<SubmoduleStatus>;

} // namespace GitComplex::Repository::Tree::Entity::Std
