/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Service/Std/Registry.h>
#include <GitComplex/Repository/Entity/Std/Concrete/BlockingIdProvider.h>
#include <GitComplex/Repository/Scanner/Entity/Std/Concrete/FileSystem.h>
#include <GitComplex/Repository/Shell/Entity/Std/Concrete/CommandExecutor.h>

/*!
 * \file include/GitComplex/Repository/Service/Std/Concrete/Registry.h
 * \brief include/GitComplex/Repository/Service/Std/Concrete/Registry.h
 */

namespace GitComplex::Repository::Service::Std::Concrete
{

/*!
 * \class Registry
 * \brief Registry.
 *
 * \headerfile GitComplex/Repository/Service/Std/Concrete/Registry.h
 *
 * Registry.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Interactor]] [[Framework::Std]]
 */
class Registry final : public Std::Registry
{
private:
    using ConcreteIdProvider        = Entity::Std::Concrete::BlockingIdProvider;
    using ConcreteFileSystemScanner = Scanner::Entity::Std::Concrete::FileSystem;
    using ConcreteCommandExecutor   = Shell::Entity::Std::Concrete::CommandExecutor;

public:
    using ShellExecutor = ::GitComplex::Shell::Executor::Entity::Std::Synchronous;
    using TaskExecutor  = Execution::Executor;

    Registry(otn::raw::weak_single<const TaskExecutor>  task_executor,
             otn::raw::weak_single<const ShellExecutor> shell_executor)
        : m_id_provider{otn::itself_type<ConcreteIdProvider>},
          m_file_system_scanner{otn::itself,
                                task_executor,
                                shell_executor,
                                m_id_provider},
          m_command_executor{otn::itself_type<ConcreteCommandExecutor>,
                             task_executor,
                             shell_executor,
                             m_id_provider}
    {}

private:
    otn::raw::weak_single<const IdProvider>       idProvider() const override
    { return m_id_provider; }
    otn::raw::weak_single<const StructureScanner> structureScanner() const override
    { return m_file_system_scanner; }
    otn::raw::weak_single<const StatusProvider>   statusProvider() const override
    { return m_file_system_scanner; }
    otn::raw::weak_single<const CommandExecutor>  commandExecutor() const override
    { return m_command_executor; }

    otn::slim::unique_single<IdProvider>      m_id_provider;
    otn::slim::unique_single<ConcreteFileSystemScanner> m_file_system_scanner;
    otn::slim::unique_single<CommandExecutor> m_command_executor;
};

} // namespace GitComplex::Repository::Service::Std::Concrete
