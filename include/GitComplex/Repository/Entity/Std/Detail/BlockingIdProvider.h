/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Entity/Std/Detail/Namespace.h>
#include <GitComplex/Repository/Entity/Std/Id.h>
#include <GitComplex/Core/Std/Path.h>

#include <unordered_map>
#include <mutex>

/*!
 * \file include/GitComplex/Repository/Entity/Std/Detail/BlockingIdProvider.h
 * \brief include/GitComplex/Repository/Entity/Std/Detail/BlockingIdProvider.h
 */

namespace GitComplex::Repository::Entity::Std::Detail
{

/*!
 * \class BlockingIdProvider
 * \brief BlockingIdProvider.
 *
 * \headerfile GitComplex/Repository/Entity/Std/Detail/BlockingIdProvider.h
 *
 * BlockingIdProvider.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Std]]
 */
class BlockingIdProvider : protected Id::Factory
{
public:
    SingleId id(const Path& path) const;
    IdList   ids(const PathList& paths) const;

    Path     path(const SingleId& id) const;

protected:
    SingleId getOrInsertId(Path path) const;
    Path     getPath(const SingleId& id) const;

    SingleId makeSingleId(const Path& path) const;

private:
    /*!
     * \struct PathHash
     * \brief PathHash.
     *
     * \headerfile GitComplex/Repository/Entity/Std/Detail/BlockingIdProvider.h
     *
     * PathHash.
     */
    struct PathHash
    {
        std::size_t operator()(Path const& path) const noexcept
        { return std::filesystem::hash_value(path); }
    };

    using Storage = std::unordered_map<Path, SingleId, PathHash>;
    mutable Storage    m_storage;
    mutable std::mutex m_mutex;
};

} // namespace GitComplex::Repository::Entity::Std::Detail
