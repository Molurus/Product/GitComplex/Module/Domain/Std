/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Scanner/Entity/Std/Detail/Namespace.h>
#include <GitComplex/Repository/Tree/Entity/Std/Structure.h>
#include <GitComplex/Repository/Tree/Entity/Std/Status.h>
#include <GitComplex/Repository/Tree/Entity/Std/SubmoduleStatus.h>
#include <GitComplex/Core/ReceiveAction.h>

#include <GitComplex/Repository/Shell/Entity/Std/Detail/ShellUser.h>
#include <GitComplex/Repository/Entity/Std/Detail/IdProviderUser.h>

/*!
 * \file include/GitComplex/Repository/Scanner/Entity/Std/Detail/SyncFileSystem.h
 * \brief include/GitComplex/Repository/Scanner/Entity/Std/Detail/SyncFileSystem.h
 */

namespace GitComplex::Repository::Scanner::Entity::Std::Detail
{

/*!
 * \class SyncFileSystem
 * \brief SyncFileSystem.
 *
 * \headerfile GitComplex/Repository/Scanner/Entity/Std/Detail/SyncFileSystem.h
 *
 * SyncFileSystem.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Std]]
 */
class SyncFileSystem : protected Shell::Entity::Std::Detail::ShellUser,
                       protected IdProviderUser
{
public:
    using ShellUser::ShellExecutor;

    SyncFileSystem(
        otn::raw::weak_single<const ShellExecutor> shell_executor,
        otn::raw::weak_single<const IdProvider>    id_provider)
        : ShellUser{std::move(shell_executor)},
          IdProviderUser{std::move(id_provider)}
    {}

    Structure::List requestStructureScan(PathList paths) const;
    StatusList      requestStatus(IdList ids) const;

protected:
    bool checkPath(const Path& path) const;
    bool isGitRepositoryRoot(const Path& path) const;
    DirectoryState detectDirectoryState(const Path& path) const;

    //
    SubmoduleStatusList submoduleStatuses(const Path& path) const;
    SubmoduleStatusList parseSubmoduleStatus(
        const Command::Output::Lines& status_strings) const;
    SubmoduleStatus     parseSubmoduleStatus(
        std::string_view status_string) const;

    Branch retrieveBranch(const Path& path) const;
    bool   retrieveHasModifications(const Path& path) const;

    //
    void updateStatusBranch(const Path& superproject_path,
                            SubmoduleStatusList& statuses) const;
    void updateStatusModification(const Path& superproject_path,
                                  SubmoduleStatusList& statuses) const;

    Structure scanStructure(const Path& path) const;
    std::optional<Status> retrieveStatus(const SingleId& id) const;
};

} // namespace GitComplex::Repository::Scanner::Entity::Std::Detail
