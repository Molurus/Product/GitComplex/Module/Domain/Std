/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Scanner/Entity/Std/Detail/SyncFileSystem.h>
#include <GitComplex/Execution/Executor.h>

#include <GitComplex/Domain/Std/Export.h>

/*!
 * \file include/GitComplex/Repository/Scanner/Entity/Std/Detail/AsyncFileSystem.h
 * \brief include/GitComplex/Repository/Scanner/Entity/Std/Detail/AsyncFileSystem.h
 */

namespace GitComplex::Repository::Scanner::Entity::Std::Detail
{

/*!
 * \class AsyncFileSystem
 * \brief AsyncFileSystem.
 *
 * \headerfile GitComplex/Repository/Scanner/Entity/Std/Detail/AsyncFileSystem.h
 *
 * AsyncFileSystem.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Std]]
 */
class GITCOMPLEX_DOMAIN_STD_EXPORT
        AsyncFileSystem
{
public:
    AsyncFileSystem(
        otn::raw::weak_single<const Execution::Executor> task_executor,
        otn::raw::weak_single<const SyncFileSystem::ShellExecutor> shell_executor,
        otn::raw::weak_single<const IdProvider> id_provider)
        : m_task_executor{std::move(task_executor)},
          m_sync_provider{otn::itself,
                          std::move(shell_executor),
                          std::move(id_provider)}
    {}

    void requestStructureScan(PathList paths,
                              ReceiveAction<Structure::List> receive_action) const;
    void requestStatus(IdList ids,
                       ReceiveAction<StatusList> receive_action) const;

private:
    otn::raw::weak_single<const Execution::Executor> m_task_executor;
    otn::unique_single<SyncFileSystem> m_sync_provider;
};

} // namespace GitComplex::Repository::Scanner::Entity::Std::Detail
