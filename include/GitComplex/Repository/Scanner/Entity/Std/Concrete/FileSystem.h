/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Tree/Entity/Std/StatusProvider.h>
#include <GitComplex/Repository/Scanner/Entity/Std/FileSystem.h>
#include <GitComplex/Repository/Scanner/Entity/Std/Detail/AsyncFileSystem.h>

/*!
 * \file include/GitComplex/Repository/Scanner/Entity/Std/Concrete/FileSystem.h
 * \brief include/GitComplex/Repository/Scanner/Entity/Std/Concrete/FileSystem.h
 */

namespace GitComplex::Repository::Scanner::Entity::Std::Concrete
{

/*!
 * \class FileSystem
 * \brief FileSystem.
 *
 * \headerfile GitComplex/Repository/Scanner/Entity/Std/Concrete/FileSystem.h
 *
 * FileSystem.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Entity]] [[Framework::Std]]
 */
class FileSystem final : public Std::FileSystem,
                         public StatusProvider
{
public:
    using ShellExecutor = const ::GitComplex::Shell::Executor::Entity::Std::Synchronous;
    using TaskExecutor  = const Execution::Executor;

    FileSystem(otn::raw::weak_single<TaskExecutor>     task_executor,
               otn::raw::weak_single<ShellExecutor>    shell_executor,
               otn::raw::weak_single<const IdProvider> id_provider)
        : m_detail{std::move(task_executor),
                   std::move(shell_executor),
                   std::move(id_provider)}
    {}

private:
    void scan(PathList paths,
              ReceiveAction<Structure::List> receive_action) const override
    {
        m_detail.requestStructureScan(std::move(paths),
                                      std::move(receive_action));
    }

    void requestStatus(IdList ids,
                       ReceiveAction<StatusList> receive_action) const override
    { m_detail.requestStatus(std::move(ids), std::move(receive_action)); }

    Detail::AsyncFileSystem m_detail;
};

} // namespace GitComplex::Repository::Scanner::Entity::Std::Concrete
