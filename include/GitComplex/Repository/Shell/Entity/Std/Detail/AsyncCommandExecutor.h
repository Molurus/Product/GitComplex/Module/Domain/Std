/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Shell/Entity/Std/Detail/SyncCommandExecutor.h>
#include <GitComplex/Core/ReceiveAction.h>
#include <GitComplex/Execution/Executor.h>

#include <GitComplex/Domain/Std/Export.h>

/*!
 * \file include/GitComplex/Repository/Shell/Entity/Std/Detail/AsyncCommandExecutor.h
 * \brief include/GitComplex/Repository/Shell/Entity/Std/Detail/AsyncCommandExecutor.h
 */

namespace GitComplex::Repository::Shell::Entity::Std::Detail
{

/*!
 * \class AsyncCommandExecutor
 * \brief AsyncCommandExecutor.
 *
 * \headerfile GitComplex/Repository/Shell/Entity/Std/Detail/AsyncCommandExecutor.h
 *
 * AsyncCommandExecutor.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Std]]
 */
class GITCOMPLEX_DOMAIN_STD_EXPORT
        AsyncCommandExecutor
{
public:
    using ShellExecutor = const ::GitComplex::Shell::Executor::Entity::Std::Synchronous;
    using TaskExecutor  = const Execution::Executor;

    AsyncCommandExecutor(otn::raw::weak_single<TaskExecutor>     task_executor,
                         otn::raw::weak_single<ShellExecutor>    shell_executor,
                         otn::raw::weak_single<const IdProvider> id_provider)
        : m_task_executor{std::move(task_executor)},
          m_sync_executor{otn::itself,
                          std::move(shell_executor),
                          std::move(id_provider)}
    {}

    void executeCommand(Command::Line command, IdList ids,
                        ReceiveAction<Command::SummaryList> receive_action) const;

    void openTerminal(SingleId id) const;
    void showModifications(SingleId id) const;
    void showLog(SingleId id) const;

private:
    otn::raw::weak_single<TaskExecutor>     m_task_executor;
    otn::unique_single<SyncCommandExecutor> m_sync_executor;
};

} // namespace GitComplex::Repository::Shell::Entity::Std::Detail
