/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Shell/Entity/Std/Detail/Namespace.h>
#include <GitComplex/Shell/Executor/Entity/Std/Synchronous.h>
#include <GitComplex/Shell/Executor/Entity/Std/Concrete/Strategy/LuckyComplete.h>
#include <GitComplex/Shell/Command/Std/Line.h>
#include <GitComplex/Shell/Command/Std/Output.h>
#include <GitComplex/Core/Std/Path.h>

#include <otn/all.hpp>

/*!
 * \file include/GitComplex/Repository/Shell/Entity/Std/Detail/ShellUser.h
 * \brief include/GitComplex/Repository/Shell/Entity/Std/Detail/ShellUser.h
 */

namespace GitComplex::Repository::Shell::Entity::Std::Detail
{

namespace SystemExecutor = ::GitComplex::Shell::Executor::Entity::Std;
namespace Command        = ::GitComplex::Shell::Command::Std;

/*!
 * \class ShellUser
 * \brief ShellUser.
 *
 * \headerfile GitComplex/Repository/Shell/Entity/Std/Detail/ShellUser.h
 *
 * ShellUser.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Std]]
 */
class ShellUser
{
public:
    using ShellExecutor    = SystemExecutor::Synchronous;
    using ExecutorStrategy = SystemExecutor::Strategy;
    using DefaultStrategy  = SystemExecutor::Concrete::Strategy::LuckyComplete;

    explicit ShellUser(otn::raw::weak_single<const ShellExecutor> shell_executor)
        : m_shell_executor{std::move(shell_executor)}
    {}

    Command::Summary
    execShellCommand(const Command::Line& command,
                     const Path& working_directory    = {},
                     const ExecutorStrategy& strategy = DefaultStrategy{}) const
    {
        return (*m_shell_executor).execute(command,
                                           working_directory,
                                           strategy);
    }

private:
    otn::raw::weak_single<const ShellExecutor> m_shell_executor;
};

} // namespace GitComplex::Repository::Shell::Entity::Std::Detail
