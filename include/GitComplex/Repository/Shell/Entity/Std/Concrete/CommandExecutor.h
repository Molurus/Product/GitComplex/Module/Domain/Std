/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Shell/Entity/Std/CommandExecutor.h>
#include <GitComplex/Repository/Shell/Entity/Std/Detail/AsyncCommandExecutor.h>

/*!
 * \file include/GitComplex/Repository/Shell/Entity/Std/Concrete/CommandExecutor.h
 * \brief include/GitComplex/Repository/Shell/Entity/Std/Concrete/CommandExecutor.h
 */

namespace GitComplex::Repository::Shell::Entity::Std::Concrete
{

/*!
 * \class CommandExecutor
 * \brief CommandExecutor.
 *
 * \headerfile GitComplex/Repository/Shell/Entity/Std/Concrete/CommandExecutor.h
 *
 * CommandExecutor.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Entity]] [[Framework::Std]]
 */
class CommandExecutor final : public Std::CommandExecutor
{
public:
    using ShellExecutor = const ::GitComplex::Shell::Executor::Entity::Std::Synchronous;
    using TaskExecutor  = const Execution::Executor;

    CommandExecutor(otn::raw::weak_single<TaskExecutor>     task_executor,
                    otn::raw::weak_single<ShellExecutor>    shell_executor,
                    otn::raw::weak_single<const IdProvider> id_provider)
        : m_detail{std::move(task_executor),
                   std::move(shell_executor),
                   std::move(id_provider)}
    {}

private:
    void executeCommand(
        Command::Line command, IdList ids,
        ReceiveAction<Command::SummaryList> receive_action) const override
    {
        m_detail.executeCommand(std::move(command), std::move(ids),
                                std::move(receive_action));
    }

    Detail::AsyncCommandExecutor m_detail;
};

} // namespace GitComplex::Repository::Shell::Entity::Std::Concrete
