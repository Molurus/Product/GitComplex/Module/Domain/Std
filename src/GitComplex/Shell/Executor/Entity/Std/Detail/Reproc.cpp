/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Shell/Executor/Entity/Std/Detail/Reproc.h>
#include <GitComplex/Converter/To.h>

#include <reproc++/reproc.hpp>
#include <reproc++/sink.hpp>

#include <algorithm>
#include <sstream>

/*!
 * \file src/GitComplex/Shell/Executor/Entity/Std/Detail/Reproc.cpp
 * \brief src/GitComplex/Shell/Executor/Entity/Std/Detail/Reproc.cpp
 */

namespace GitComplex::Converter
{

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Std]]
 */
template <class From>
inline
auto convert(From&& cleanup,
             To<::reproc::cleanup> = {},
             RequireSame<From, Shell::Executor::Entity::Std::Cleanup::Action> = {})
{
    using Action = Shell::Executor::Entity::Std::Cleanup::Action;
    switch (cleanup)
    {
    case Action::None:
        return ::reproc::cleanup::noop;
    case Action::Wait:
        return ::reproc::cleanup::wait;
    case Action::Terminate:
        return ::reproc::cleanup::terminate;
    case Action::Kill:
        return ::reproc::cleanup::kill;
    }

    return ::reproc::cleanup::noop;
}

} // namespace GitComplex::Converter

namespace GitComplex::Shell::Executor::Entity::Std::Detail
{

namespace
{

#ifdef _WIN32
#else
const std::string::value_type Space{' '};
const std::string::value_type Quote{'\"'};

template <class InputIt>
InputIt findSpace(InputIt first, InputIt last)
{
    return std::find(first, last, Space);
}

template <class InputIt>
InputIt findNonSpace(InputIt first, InputIt last)
{
    return std::find_if_not(first, last,
                            [](const auto& value) { return value == Space; });
}

template <class InputIt>
InputIt findClosingQuote(InputIt first, InputIt last)
{
    return std::find(first, last, Quote);
}
#endif

// TODO: Move the parseArguments() to the Command::ArgumentParser.
std::vector<std::string> parseArguments(const Command::Line& command)
{
    if (command.empty())
        return {};

    std::vector<std::string> args;
#ifdef _WIN32
    args.push_back(command);
#else
    using Iterator = Command::Line::const_iterator;
    const Iterator end   = command.cend();
    Iterator       first = findNonSpace(command.cbegin(), end);
    while (first != end)
    {
        Iterator last;
        bool     is_quoted = *first == Quote;
        if (is_quoted)
        {
            ++first; // Skip the opening quote.
            last = findClosingQuote(first, end);
        }
        else
        {
            last = findSpace(first + 1, end);
        }

        args.push_back(std::string{first, last});

        // Skip the closing quote.
        if (is_quoted && (last != end))
            ++last;

        first = findNonSpace(last, end);
    }
#endif

    return args;
}

Command::Output::Lines adaptOutput(const std::string& output)
{
    Command::Output::Line  line;
    Command::Output::Lines lines;
    std::istringstream     stream(output);
    while (getline(stream, line))
        lines.push_back(line);

    return lines;
}

} // namespace

Command::Summary Reproc::execute(const Command::Line& command,
                                 const Path&     working_directory,
                                 const Strategy& strategy) const
{
    using namespace std::chrono_literals;

    const auto  args = parseArguments(command);
    std::string work_dir_string = working_directory.string();

    Command::Summary summary{command,
                             working_directory,
                             {}, 0, {}};

    do
    {
        if (args.empty())
        {
            summary.error =
                std::make_error_code(std::errc::no_such_file_or_directory);
            continue;
        }

        ::reproc::process shell(Converter::convert(strategy.cleanup().action),
                                strategy.cleanup().waitTimeout);
        summary.error = shell.start(args, &work_dir_string);
        if (summary.error)
            continue;

        summary.error = shell.wait(strategy.waitTimeout());
        if (summary.error)
            continue;

        summary.exitStatus = shell.exit_status();

        std::string output_result;
        summary.error = shell.drain(::reproc::stream::out,
                                    ::reproc::string_sink(output_result));
        if (summary.error)
            continue;

        summary.output.result = adaptOutput(output_result);

        std::string output_error;
        summary.error = shell.drain(::reproc::stream::err,
                                    ::reproc::string_sink(output_error));
        if (summary.error)
            continue;

        summary.output.error = adaptOutput(output_error);
    }
    while (strategy.repeatOn(summary.error));

    return summary;
}

} // namespace GitComplex::Shell::Executor::Entity::Std::Detail
