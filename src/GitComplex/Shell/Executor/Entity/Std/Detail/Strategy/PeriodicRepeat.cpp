/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Shell/Executor/Entity/Std/Detail/Strategy/PeriodicRepeat.h>
#include <GitComplex/Shell/Executor/Entity/Std/Detail/Strategy/Namespace.h>
#include <GitComplex/Shell/Command/Std/Failure.h>

/*!
 * \file src/GitComplex/Shell/Executor/Entity/Std/Detail/Strategy/PeriodicRepeat.cpp
 * \brief src/GitComplex/Shell/Executor/Entity/Std/Detail/Strategy/PeriodicRepeat.cpp
 */

namespace GitComplex::Shell::Executor::Entity::Std::Detail::Strategy
{

std::chrono::milliseconds PeriodicRepeat::waitTimeout() const noexcept
{
    return m_wait_timeout;
}

bool PeriodicRepeat::repeatOn(const std::error_code& error) const noexcept
{
    bool repeat = --m_attempt > 0 && error == Command::Failure::WaitTimeout;
    return repeat;
}

Cleanup PeriodicRepeat::cleanup() const noexcept
{
    return {Cleanup::Action::None, std::chrono::milliseconds::zero()};
}

} // namespace GitComplex::Shell::Executor::Entity::Std::Detail::Strategy
