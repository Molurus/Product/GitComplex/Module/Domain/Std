/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Shell/Command/Std/Failure.h>
#include <reproc++/error.hpp>

/*!
 * \file src/GitComplex/Shell/Command/Std/Failure.cpp
 * \brief src/GitComplex/Shell/Command/Std/Failure.cpp
 */

namespace GitComplex::Shell::Command::Std
{

class FailureCategory : public std::error_category
{
public:
    const char* name() const noexcept override;
    std::string message(int condition) const override;
    bool equivalent(const std::error_code& code,
                    int condition) const noexcept override;
};

const char* FailureCategory::name() const noexcept
{
    return "GitComplex shell command";
}

std::string FailureCategory::message(int condition) const
{
    switch (static_cast<Failure>(condition))
    {
    case Failure::WaitTimeout:
        return "wait timeout";
    }

    return "(unrecognized condition)";
}

bool FailureCategory::equivalent(const std::error_code& code,
                                 int condition) const noexcept
{
    switch (static_cast<Failure>(condition))
    {
    case Failure::WaitTimeout:
        return code == ::reproc::error::wait_timeout;
    }

    return false;
}

const FailureCategory FailureCategoryInstance{};

std::error_condition make_error_condition(Failure condition)
{
    return {static_cast<int>(condition), FailureCategoryInstance};
}

} // namespace GitComplex::Shell::Command::Std
