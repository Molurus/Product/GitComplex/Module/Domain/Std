/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Tool/Std/SuperprojectDetector.h>
#include <GitComplex/Shell/Executor/Entity/Std/Concrete/Default.h>
#include <GitComplex/Shell/Executor/Entity/Std/Concrete/Strategy/LuckyComplete.h>

#include <otn/all.hpp>

/*!
 * \file src/GitComplex/Tool/Std/SuperprojectDetector.cpp
 * \brief src/GitComplex/Tool/Std/SuperprojectDetector.cpp
 */

namespace GitComplex::Tool::Std
{

SuperprojectDetector::SuperprojectDetector()
    : SuperprojectDetector{std::filesystem::current_path()}
{}

SuperprojectDetector::SuperprojectDetector(const Path& path)
{
    namespace fs       = std::filesystem;
    namespace Executor = Shell::Executor::Entity::Std;
    namespace Command  = Shell::Command::Std;

    otn::slim::unique_single<Executor::Synchronous> shell_executor{
        otn::itself_type<Executor::Concrete::Default>};
    Executor::Concrete::Strategy::LuckyComplete strategy;

    Command::Line command = "git rev-parse --show-toplevel";
    auto summary = (*shell_executor).execute(command, path, strategy);

    if (summary.output.result.empty())
        return; // The 'path' is not a git repository.

    Path working_tree = summary.output.result.at(0);

    if (!fs::exists(working_tree / ".gitmodules"))
    // The 'working_tree' is not a git superproject.
    {
        command = "git rev-parse --show-superproject-working-tree";
        summary = (*shell_executor).execute(command, working_tree, strategy);
        if (summary.output.result.empty())
            return; // The 'working_tree' is not a git submodule.

        working_tree = summary.output.result.at(0);
    }

    m_working_tree = working_tree;

    command = "git rev-parse --absolute-git-dir";
    summary = (*shell_executor).execute(command, working_tree, strategy);
    if (summary.output.result.empty())
        return; // The 'working_tree' has no .git directory.

    m_git_dir = summary.output.result.at(0);
}

} // namespace GitComplex::Tool::Std
