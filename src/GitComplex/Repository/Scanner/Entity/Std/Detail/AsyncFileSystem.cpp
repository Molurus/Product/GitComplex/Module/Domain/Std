/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Scanner/Entity/Std/Detail/AsyncFileSystem.h>
#include <GitComplex/Core/FunctorBuilder.h>

/*!
 * \file src/GitComplex/Repository/Scanner/Entity/Std/Detail/AsyncFileSystem.cpp
 * \brief src/GitComplex/Repository/Scanner/Entity/Std/Detail/AsyncFileSystem.cpp
 */

namespace GitComplex::Repository::Scanner::Entity::Std::Detail
{

void AsyncFileSystem::requestStructureScan(
    PathList paths, ReceiveAction<Structure::List> receive_action) const
{
    (*m_task_executor).execute(
        makeActionFunctor(std::move(receive_action),
                          otn::conform::weak(m_sync_provider),
                          &SyncFileSystem::requestStructureScan,
                          std::move(paths)));
}

void AsyncFileSystem::requestStatus(
    IdList ids, ReceiveAction<StatusList> receive_action) const
{
    (*m_task_executor).execute(
        makeActionFunctor(std::move(receive_action),
                          otn::conform::weak(m_sync_provider),
                          &SyncFileSystem::requestStatus,
                          std::move(ids)));
}

} // namespace GitComplex::Repository::Scanner::Entity::Std::Detail
