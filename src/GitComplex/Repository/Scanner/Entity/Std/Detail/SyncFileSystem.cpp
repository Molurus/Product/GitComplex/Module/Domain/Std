/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Scanner/Entity/Std/Detail/SyncFileSystem.h>
#include <GitComplex/Shell/Executor/Entity/Std/Concrete/Strategy/PeriodicRepeat.h>

/*!
 * \file src/GitComplex/Repository/Scanner/Entity/Std/Detail/SyncFileSystem.cpp
 * \brief src/GitComplex/Repository/Scanner/Entity/Std/Detail/SyncFileSystem.cpp
 */

namespace GitComplex::Repository::Scanner::Entity::Std::Detail
{

namespace SystemExecutor = ::GitComplex::Shell::Executor::Entity::Std;

inline
SystemExecutor::Concrete::Strategy::PeriodicRepeat repeat_3_5()
{
    using namespace std::chrono_literals;
    return SystemExecutor::Concrete::Strategy::PeriodicRepeat{3u, 5s};
}

inline
SystemExecutor::Concrete::Strategy::PeriodicRepeat repeat_3_30()
{
    using namespace std::chrono_literals;
    return SystemExecutor::Concrete::Strategy::PeriodicRepeat{3u, 30s};
}

Structure::List SyncFileSystem::requestStructureScan(PathList paths) const
{
    Structure::List structures;

    for (const auto& path : paths)
        structures.push_back(scanStructure(path));

    return structures;
}

StatusList SyncFileSystem::requestStatus(IdList ids) const
{
    StatusList statuses;

    for (const auto& id : ids)
        if (auto status = retrieveStatus(id))
            statuses.push_back(std::move(*status));

    return statuses;
}

bool SyncFileSystem::checkPath(const Path& path) const
{
    return(std::filesystem::exists(path));
}

bool SyncFileSystem::isGitRepositoryRoot(const Path& path) const
{
    return(std::filesystem::exists(path / ".git"));
}

DirectoryState SyncFileSystem::detectDirectoryState(const Path& path) const
{
    if (!checkPath(path))
        return DirectoryState::NotExist;

    if (!isGitRepositoryRoot(path))
        return DirectoryState::NotGit;

    return DirectoryState::WorkTree;
}

Structure SyncFileSystem::scanStructure(const Path& path) const
{
    Structure structure{idProvider().id(path),
                        path,
                        detectDirectoryState(path)};

    if (structure.directoryState == DirectoryState::WorkTree)
    {
        auto submodules = submoduleStatuses(path);
        for (const auto& submodule : submodules)
        {
            // TODO: Scan recursively.
            Path full_path = path / submodule.path;
            structure.submodules.push_back(
                Structure{idProvider().id(full_path),
                          full_path,
                          detectDirectoryState(full_path),
                          structure.id});
        }
    }

    return structure;
}

std::optional<Status> SyncFileSystem::retrieveStatus(const SingleId& id) const
{
    Path path = idProvider().path(id);
    if (detectDirectoryState(path) != DirectoryState::WorkTree)
        return {};

    Branch branch = retrieveBranch(path);
    bool   has_modifications = retrieveHasModifications(path);

    return Status{id, branch, {}, has_modifications};
}

SubmoduleStatusList
SyncFileSystem::submoduleStatuses(const Path& path) const
{
    if (detectDirectoryState(path) != DirectoryState::WorkTree)
        return {};

    const auto summary =
        execShellCommand("git submodule status", path, repeat_3_30());
    if (summary.error)
        return {};

    auto statuses = parseSubmoduleStatus(summary.output.result);
    updateStatusBranch(path, statuses);
    updateStatusModification(path, statuses);
    return statuses;
}

SubmoduleStatusList
SyncFileSystem::parseSubmoduleStatus(
    const ::GitComplex::Shell::Command::Std::Output::Lines& status_strings) const
{
    std::vector<SubmoduleStatus> statuses;
    for (const auto& s : status_strings)
        statuses.push_back(parseSubmoduleStatus(s));
    return statuses;
}

SubmoduleStatus
SyncFileSystem::parseSubmoduleStatus(std::string_view status_string) const
{
    SubmoduleStatus status;

    auto i = status_string.begin();
    status.prefix = *i;
    auto j = std::find(++i, status_string.end(), char{' '});
    status.sha = {i, j};
    i = j;
    j = std::find(++i, status_string.end(), char{' '});
    status.path = std::string{i, j};
    if (j != status_string.end())
        status.branch = {++j, status_string.end()};

    return status;
}

Branch SyncFileSystem::retrieveBranch(const Path& path) const
{
    const auto summary =
        execShellCommand("git rev-parse --abbrev-ref HEAD", path, repeat_3_5());

    if (!summary.output.result.empty())
        return summary.output.result[0];

    return {};
}

bool SyncFileSystem::retrieveHasModifications(const Path& path) const
{
    const auto summary = execShellCommand("git status -s", path, repeat_3_5());
    return !summary.output.result.empty();
}

void SyncFileSystem::updateStatusBranch(const Path& superproject_path,
                                        SubmoduleStatusList& statuses) const
{
    for (auto& submodule_status : statuses)
    {
        const auto summary =
            execShellCommand("git rev-parse --abbrev-ref HEAD",
                             superproject_path / submodule_status.path,
                             repeat_3_5());
        if (!summary.output.result.empty())
            submodule_status.branch = summary.output.result[0];
    }
}

void SyncFileSystem::updateStatusModification(
    const Path& superproject_path, SubmoduleStatusList& statuses) const
{
    for (auto& submodule_status : statuses)
    {
        const auto summary =
            execShellCommand("git status -s",
                             superproject_path / submodule_status.path,
                             repeat_3_5());
        submodule_status.hasModifications = !summary.output.result.empty();
    }
}

} // namespace GitComplex::Repository::Scanner::Entity::Std::Detail
