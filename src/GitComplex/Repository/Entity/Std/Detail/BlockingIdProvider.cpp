/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Entity/Std/Detail/BlockingIdProvider.h>

/*!
 * \file src/GitComplex/Repository/Entity/Std/Detail/BlockingIdProvider.cpp
 * \brief src/GitComplex/Repository/Entity/Std/Detail/BlockingIdProvider.cpp
 */

namespace GitComplex::Repository::Entity::Std::Detail
{

SingleId BlockingIdProvider::id(const Path& path) const
{
    std::lock_guard<std::mutex> guard{m_mutex};

    return getOrInsertId(path);
}

IdList BlockingIdProvider::ids(const PathList& paths) const
{
    IdList ids;

    std::lock_guard<std::mutex> guard{m_mutex};
    for (auto& path : paths)
        ids.push_back(getOrInsertId(path));

    return ids;
}

Path BlockingIdProvider::path(const SingleId& id) const
{
    std::lock_guard<std::mutex> guard{m_mutex};

    return getPath(id);
}

SingleId BlockingIdProvider::getOrInsertId(Path path) const
{
    auto it = m_storage.find(path);
    if (it != m_storage.end())
        return it->second;

    return (*m_storage.emplace(path, makeSingleId(path)).first).second;
}

// TODO: Move __PRETTY_FUNCTION__ to Support.
#ifndef __PRETTY_FUNCTION__
#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif
#endif

Path BlockingIdProvider::getPath(const SingleId& id) const
{
    auto it = std::find_if(std::begin(m_storage), std::end(m_storage),
                           [&](auto& kv) { return kv.second == id; });

    if (it == std::end(m_storage))
        throw std::out_of_range{__PRETTY_FUNCTION__};

    return it->first;
}

SingleId BlockingIdProvider::makeSingleId(const Path& path) const
{
    return makeId(PathHash{} (path));
}

} // namespace GitComplex::Repository::Entity::Std::Detail
